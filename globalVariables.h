//*********************************************************************
// Autors:                    email:                                 **
// Tefera Yonas Teodros      yonasteodros.tefera@studenti.unitn.it   **
// Zegeye Bayable Teshome    bayable.zegeye@studenti.unitn.it        **
//                                                                   **
//date 08/2016                                                       **
//*********************************************************************


#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H

#include <QObject>
#include <QtNetwork/qabstractsocket.h>
#include <QtNetwork/qnetworkaccessmanager.h>
#include <QtNetwork/qnetworkrequest.h>

using namespace std;
extern QNetworkAccessManager *manager;
extern int frameCount;
extern string parameter;

#endif // GLOBALVARIABLES_H
