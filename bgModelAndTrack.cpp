//*********************************************************************
// Autors:                    email:                                 **
// Tefera Yonas Teodros      yonasteodros.tefera@studenti.unitn.it   **
// Zegeye Bayable Teshome    bayable.zegeye@studenti.unitn.it        **
//                                                                   **
//date 08/2016                                                       **
//*********************************************************************

#include "bgModelAndTrack.h"
#include "TargetObject.h"

void trackObject(Mat& frame, vector<TargetObject>& myObjects, double dT, Ptr<BackgroundSubtractor>& pMOG2){
    Mat res, fgMaskMOG2;


    rectangle(frame, Point(100, 100), Point(540, 380), CV_RGB(0,0,255), 1);

    frame.copyTo( res );

    // >>>> Predict
    unsigned int size = myObjects.size();
    cout<<size <<" objects moving"<<endl;
    for (unsigned int i = 0; i < size; i++)
    {
        myObjects[i].predictObjectState(frame, dT);
    }
    // <<<< Predict

    // >>>>> Noise smoothing
    GaussianBlur(res, res, Size(5, 5), 3.0, 3.0);
    // <<<<< Noise smoothing

    // >>>> Background Subtraction
    pMOG2->apply(res, fgMaskMOG2);
    // <<<< Background Subtraction
    // >>>>> Improving the result
    cv::erode(fgMaskMOG2, fgMaskMOG2, cv::Mat(), cv::Point(-1, -1), 2);
    cv::dilate(fgMaskMOG2, fgMaskMOG2, cv::Mat(), cv::Point(-1, -1), 2);
    // <<<<< Improving the result
    imshow("Motion2", fgMaskMOG2);
    // >>>>> Contours detection
    vector<vector<Point> > contours;
    findContours(fgMaskMOG2, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    // <<<<< Contours detection

    // >>>>> Histogram Params
    Mat hsv_base;
    // Using 50 bins for hue and 60 for saturation
    int h_bins = 50; int s_bins = 60;
    int histSize[] = { h_bins, s_bins };

    // hue varies from 0 to 179, saturation from 0 to 255
    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    // Use the o-th and 1-st channels
    int channels[] = { 0, 1 };

    // >>>>> Histogram Params
    MatND hist_base;
    // <<<<< Histogram Params

    vector<Rect> boxRect;

    int frameCols = frame.cols;
    int frameRows = frame.rows;

    for (size_t i = 0; i < contours.size(); i++)
    {
        Rect bBox;
        bBox = boundingRect(contours[i]);

        // Searching for a bBox almost square
        if (bBox.area() >= 60 && bBox.area() <= 20000)
        {
            enlargeRectangle(bBox, frameCols, frameRows);
            boxRect.push_back(bBox);
        }
    }

    //>>>>> Feature Assosiation

    if (boxRect.size() > 1)
    {
        bool overlap = mergeRectangles(boxRect);

        while(overlap && (boxRect.size() > 1))
        {
            overlap = mergeRectangles(boxRect);
        }
    }

    for (size_t j=0; j<boxRect.size(); j++)
    {
        if(frameCount==0)
        {
            rectangle(frame, boxRect[j], CV_RGB(0,255,0), 1);

            cvtColor( frame(boxRect[j]), hsv_base, COLOR_BGR2HSV );
            calcHist( &hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
            normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );

            // <<<<< Histogram Computation
            bool matchFound = featureAssociateHist(myObjects, hist_base, boxRect[j], frame(boxRect[j]));
            if (!matchFound)
            {
                TargetObject newObject(boxRect[j], hist_base, frame(boxRect[j]));
                myObjects.push_back(newObject);
            }
            frameCount = ptzControl(myObjects[0].getSnapPosition());
        }
        else
        {
            frameCount--;
            break;
        }

    }

    if(boxRect.size() == 0)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&rzoom=-500")));
    }

    size = myObjects.size();
    for (unsigned int i=0; i<size; i++)
    {
        int notFoundCount = myObjects[i].getNotFoundCount();
        if (notFoundCount >=100)
        {
            myObjects.erase(myObjects.begin() + i); //dropObject
        }
        else
        {
            myObjects[i].correctTracker();
        }
    }
    putText( frame, parameter, Point(20,20), FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(0,255,255), 1, CV_AA);

}

bool featureAssociateHist(vector<TargetObject>& newMyObjects, const MatND& hist_base, const Rect& newBox, const Mat newSnap){

    bool newMatchFound = false;
    double goodMatch = 100;
    int matchPoint;
    unsigned int size = newMyObjects.size();
    for (unsigned int i = 0; i < size; i++)
    {
        Mat PrevHist=newMyObjects[i].getHistogram();
        double base_test = compareHist( hist_base, PrevHist, 2 );

        if (base_test < goodMatch)
        {
            goodMatch = base_test;
            matchPoint = i;
        }
    }

    if (goodMatch < 50) /// tune this parameter
    {
        cout<<"object number "<< matchPoint <<" have min dist: "<< goodMatch <<endl;
        newMyObjects[matchPoint].setMeas(newBox);
        newMyObjects[matchPoint].setHistogram(hist_base);
        newMyObjects[matchPoint].setSnapPosition(newBox);
        newMyObjects[matchPoint].setSnapShot(newSnap);
        newMatchFound = true;
    }

    return newMatchFound;
}

bool mergeRectangles(vector<Rect>& boxRect)
{
    bool overlapFound = false;
    size_t length = boxRect.size();

    for (size_t i = 0; i < (length - 1); i++)
    {
        for (size_t j = i + 1; j < length; j++)
        {
            if((boxRect.size() > j) && ((boxRect[i] & boxRect[j]).area() > 0))
            {
                vector<Rect> tempRect;
                tempRect.push_back(boxRect[i] | boxRect[j]);
                boxRect.erase(boxRect.begin() + i);
                boxRect.erase(boxRect.begin() + j);
                boxRect.push_back(tempRect[0]);
                overlapFound = true;
            }

        }
    }
    return overlapFound;
}

void enlargeRectangle(Rect& bBox, int frameCols, int frameRows)
{
    int pad = 25; /// tune this parameter
    if (bBox.x > pad)
    {
        bBox.x = bBox.x - pad;
        if ((bBox.x + bBox.width) >= (frameCols - 1 - 2*pad))
        {
            bBox.width = frameCols-bBox.x;
        }
        else
        {
            bBox.width = bBox.width + 2*pad;
        }
    }
    else
    {
        if ((bBox.x + bBox.width) >= (frameCols -1 - pad))
        {
            bBox.width = frameCols-bBox.x;
        }
        else
        {
            bBox.width = bBox.width + pad;
        }
    }

    if (bBox.y > pad)
    {
        bBox.y = bBox.y - pad;
        if ((bBox.y + bBox.height) >= (frameRows -1 - 2*pad))
        {
            bBox.height = frameRows-bBox.y;
        }
        else
        {
            bBox.height = bBox.height + 2*pad;
        }
    }
    else
    {
        if ((bBox.y + bBox.height) >= (frameRows -1 - pad))
        {
            bBox.height = frameRows-bBox.y;
        }
        else
        {
            bBox.height = bBox.height + pad;
        }
    }
}

int ptzControl(Rect snapPos)
{
    int newFrameCount = 0, number = 20;
    Point pt1 = Point(100, 100);
    Point pt2 = Point(540, 380);

    if(snapPos.x < pt1.x)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=left")));
        parameter = " Pan LEFT ";
        newFrameCount = number;
    }
    else if((snapPos.x + snapPos.width) > pt2.x)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=right")));
          parameter = " Pan RIGHT ";
        newFrameCount = number;
    }

    if(snapPos.y < pt1.y)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=up")));
        parameter = " Tilt UP ";
        newFrameCount = number;
    }
    else if((snapPos.y + snapPos.height) > pt2.y)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=down")));
        parameter = " Tilt DOWN ";
        newFrameCount = number;
    }

    if(snapPos.area() < 7000)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&rzoom=+2000")));
        parameter = " Zoom +1000 ";
        newFrameCount = number;
        if(snapPos.x < pt1.x)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=left")));
            parameter = "  Pan LEFT ";
            newFrameCount = number;
        }
        else if((snapPos.x + snapPos.width) > pt2.x)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=right")));
            parameter = "  Pan RIGHT ";
            newFrameCount = number;
        }

        if(snapPos.y < pt1.y)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=up")));
            parameter = " Tilt UP ";
            newFrameCount = number;
        }
        else if((snapPos.y + snapPos.height) > pt2.y)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=down")));
            parameter = " Tilt DOWN ";
            newFrameCount = number;
        }
    }
    else if(snapPos.area() > 10000)
    {
        manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&rzoom=-500")));
        parameter = " Zoom -500 ";
        newFrameCount = number;
        if(snapPos.x < pt1.x)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=left")));
            parameter = " Zoom -500, Pan LEFT ";
            newFrameCount = number;
        }
        else if((snapPos.x + snapPos.width) > pt2.x)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=right")));
            parameter = " Zoom -500, Pan RIGHT ";
            newFrameCount = number;
        }

        if(snapPos.y < pt1.y)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=up")));
            parameter = " Zoom -500, Tilt UP ";
            newFrameCount = number;
        }
        else if((snapPos.y + snapPos.height) > pt2.y)
        {
            manager->get(QNetworkRequest(QUrl("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&move=down")));
            parameter = " Zoom -500, Tilt DOWN ";
            newFrameCount = number;
        }
    }

    return newFrameCount;
}
