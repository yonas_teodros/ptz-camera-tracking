//*********************************************************************
// Autors:                    email:                                 **
// Tefera Yonas Teodros      yonasteodros.tefera@studenti.unitn.it   **
// Zegeye Bayable Teshome    bayable.zegeye@studenti.unitn.it        **
//                                                                   **
//date 08/2016                                                       **
//*********************************************************************

#include "TargetObject.h"
#include "alarmDetection.h"
#include "globalVariables.h"



#ifndef BGMODELANDTRACK_H
#define BGMODELANDTRACK_H

void trackObject(Mat&, vector<TargetObject>&, double, Ptr<BackgroundSubtractor>&);

bool featureAssociateHist(vector<TargetObject>&, const MatND&, const Rect&, const Mat);

Rect featureAssociateSurf(const Mat, const Mat);

bool mergeRectangles(vector<Rect>&);

void enlargeRectangle(Rect&, int, int);

int ptzControl(Rect);


#endif // BGMODELANDTRACK_H
