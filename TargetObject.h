// Module "core"
#include <opencv2/core/core.hpp>

//*********************************************************************
// Autors:                    email:                                 **
// Tefera Yonas Teodros      yonasteodros.tefera@studenti.unitn.it   **
// Zegeye Bayable Teshome    bayable.zegeye@studenti.unitn.it        **
//                                                                   **
//date 08/2016                                                       **
//*********************************************************************


// Module "highgui"
#include <opencv2/highgui/highgui.hpp>

// Module "imgproc"
#include <opencv2/imgproc/imgproc.hpp>

// Module "video"
#include <opencv2/video/video.hpp>

#include "opencv2/features2d/features2d.hpp"
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include "opencv2/calib3d/calib3d.hpp"

#include <opencv2/face.hpp>
#include <opencv2/face/facerec.hpp>

// Output
#include <iostream>

// Vector
#include <vector>

using namespace std;
using namespace cv;
using namespace cv::face;
using namespace cv::xfeatures2d;


#ifndef TARGETOBJECT_H
#define TARGETOBJECT_H

class TargetObject //: public KalmanFilter
{
public:
    //Default Constructor
    TargetObject();

    // Overload constructors
    TargetObject(Rect, MatND, Mat);

    //Destructor
    ~TargetObject();

    //Accessor Functions
    KalmanFilter getTracker() const;

    Mat getState() const;

    void setState();

    Mat getMeas() const;

    void setMeas(const Rect newBox);

    int getNotFoundCount();

    MatND getHistogram();

    void setHistogram(MatND);

    Mat getSnapShot();

    void setSnapShot(Mat);

    Rect getSnapPosition();

    void setSnapPosition(Rect);

    void predictObjectState(Mat& res, double dT);

    void correctTracker();



private:
    //Member Variables

    // >>>> Kalman Filter
    int stateSize, measSize, contrSize, notFoundCount;
    unsigned int type;
    bool found;

    KalmanFilter tracker;
    Mat state;
    Mat meas;
    Mat snapShot;
    Rect snapPosition;
    // <<<< Kalman Filter
    MatND hist_base;
    // <<<< SIFT Parameters
};

#endif // TARGETOBJECT_H
