The idea of this project is first to detect a moving object and then track it on partially dynamic scene. 
This dynamic scene happens only when the camera parameters (pan, tilt and zoom) are changed.
Otherwise the scene will be static as the camera is placed in a fixed position.
In this project, we modeled and track an object from an Axis PTZ camera. Since the video streams are captured from a PTZ camera, 
background subtraction cannot provide a reliable enhancement of region of interest and good tracking outcome.
Our main con-tribution is developing a tracking strategy to track and segment object in dynamic scenes with occlusion handling by the help of kalman filter.
By calculating the objects of interest histogram and carefully modeling the background we developed the project.
The problems caused by rapid object appearance change can also be avoided due to the stability of the tracking measurement by 20 frames.
Furthermore, we set a region of freedom movment to make the tracking stable as long as the object is inside of the region 
and when the object of interest lives the region we set the Roi in the center of the region by sending the appropriate PTZ commandsto the camera.
From experiments on a videos of challenging outdoor and indoor sequences, our algorithm demonstrates accurate and reliable tracking.