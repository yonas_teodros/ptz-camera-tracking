//*********************************************************************
// Autors:                    email:                                 **
// Tefera Yonas Teodros      yonasteodros.tefera@studenti.unitn.it   **
// Zegeye Bayable Teshome    bayable.zegeye@studenti.unitn.it        **
//                                                                   **
//date 08/2016                                                       **
//*********************************************************************

#include <QCoreApplication>
#include "bgModelAndTrack.h"
#include "TargetObject.h"
#include "globalVariables.h"

vector<TargetObject> myObjects;

// Camera frame
Mat frame;
Ptr<BackgroundSubtractor> pMOG2; //MOG2 Background subtractor



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    pMOG2 = createBackgroundSubtractorMOG2(100,16,false); //MOG2 approach
    manager->get(QNetworkRequest(QUrl("http://root:mmnet@192.168.0.90")));

   // Camera Capture
   VideoCapture cap;
   cap.open("http://root:mmnet@192.168.0.90/mjpg/video.mjpg");

   cout << "\nHit 'q' to exit...\n";

   char ch = 0;

   double ticks = 0;

   // >>>>> Main loop
   while (ch != 'q' && ch != 'Q')
   {
       double precTick = ticks;
       ticks = (double) getTickCount();

       double dT = (ticks - precTick) / getTickFrequency(); //seconds

       // Frame acquisition
       cap >> frame;

       imshow("Orignal", frame);
       trackObject(frame, myObjects, dT, pMOG2);



       // Final result
       imshow("Tracking", frame);
//       imshow("Motion", rangeRes);

       // User key
       ch = waitKey(1);
   }
   cap.release();
   // <<<<< Main loop

   return a.exec();
}


